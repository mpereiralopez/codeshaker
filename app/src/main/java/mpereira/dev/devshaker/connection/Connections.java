package mpereira.dev.devshaker.connection;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import mpereira.dev.devshaker.LoginActivity;
import mpereira.dev.devshaker.QuestionTestActivity;
import mpereira.dev.devshaker.SingUpActivity;
import mpereira.dev.devshaker.SplashActivity;
import mpereira.dev.devshaker.level.CodeLanding;
import mpereira.dev.devshaker.level.LevelActivity;
import mpereira.dev.devshaker.level.LevelCompleted;
import mpereira.dev.devshaker.persistance.Subcategory;

/**
 * Created by je10413 on 07/04/2015.
 */
public class Connections {

    private static final String TAG ="CONNECTIONS";
    public final static String URL = "http://54.171.234.231/devShaker/APIDemo.php";
    //public final static String URL = "http://192.168.1.38:8888/API_Demo.php";

    public final static String CREATION = "creation";
    public final static String LOGIN = "login";
    public final static String VERIFY = "verify";

    public final static String PROFILE = "profil";
    public final static String BADGES = "badges";
    public final static String ACHIEVEMENTS = "achievements";
    public final static String FRIENDS = "friends";
    public final static String CATEGORY = "categorie";
    public final static String SUBCATEGORY = "subCategorie";
    public final static String LEVEL = "level";

    public final static String QUESTIONS = "questions";
    public final static String QUESTIONSOFSUBANDLEVEL = "questionsBySubAndLevel";
    public final static String QUESTION_SUB = "questionSub";
    public final static String UPDATE_LEVEL_FINISHED = "updateLevelFinished";
    public final static String UPDATE_COMPANIES = "updateCompaniesPos";
    public final static String UPDATE_PROFILE = "updateProfil";

    private boolean creationWasCalled = false;

    public static boolean haveNetworkConnection(Context ctx) {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

    public void callToWS(String id, String [] params,int userId){

        String userIdStr = Integer.toString(userId);

        if (params !=null){

            String superArray[][] = {{URL},{id},{userIdStr},params};
            new RequestTask().execute(superArray);


        }else{
            String superArray[][] = {{URL},{id},{userIdStr}};
            new RequestTask().execute(superArray);


        }
    }





    class RequestTask extends AsyncTask<String[], String, String> {

        private String idToPostExecute;

        @Override
        protected String doInBackground(String[]... allParams) {
            HttpClient httpclient = new DefaultHttpClient();
            HttpResponse response;
            String responseString = null;
            try {

                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

                String url = allParams[0][0];
                String id = allParams[1][0];
                String userId = allParams[2][0];
                Log.d(TAG,"allParams.length: "+allParams.length);

                if(allParams.length>3){

                    String paramsArray [] =allParams[3];
                    for (int i = 0; i < paramsArray.length; i++) {

                        nameValuePairs.add(new BasicNameValuePair("param[]",paramsArray[i]));
                        Log.d(TAG,"PARAMS "+paramsArray[i]);

                    }


                }
                idToPostExecute = id;
                Log.d(TAG,"URL: "+url);
                Log.d(TAG,"ID: "+id);

                nameValuePairs.add(new BasicNameValuePair("id", id));
                if(Integer.parseInt(userId) != 0){
                    System.out.println("USER "+userId);
                    nameValuePairs.add(new BasicNameValuePair("user", userId));

                }

                HttpPost httpPost = new HttpPost(url);

                httpPost.setHeader(HTTP.CONTENT_TYPE,
                        "application/x-www-form-urlencoded;charset=UTF-8");

                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));

                /*HttpParams params = new BasicHttpParams();
                params.setParameter("id", id);
                params.setParameter("data",allParams[2]);
                httpPost.setParams(params);*/

                response = httpclient.execute(httpPost);
                StatusLine statusLine = response.getStatusLine();
                if (statusLine.getStatusCode() == HttpStatus.SC_OK) {
                    ByteArrayOutputStream out = new ByteArrayOutputStream();
                    response.getEntity().writeTo(out);
                    responseString = out.toString();
                    out.close();
                } else {
                    //Closes the connection.
                    response.getEntity().getContent().close();
                    throw new IOException(statusLine.getReasonPhrase());
                }
            } catch (ClientProtocolException e) {
                //TODO Handle problems..
            } catch (IOException e) {
                //TODO Handle problems..
                System.out.println("ERROR 2");
                Log.e("HTTP ERROR", e.getMessage());
            }
            return responseString;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            //Do anything with response..
            System.out.println(result);


            try {
                JSONArray jsonArray = new JSONArray(result);
                if(idToPostExecute == Connections.SUBCATEGORY){
                    CodeLanding.setSubCategoriesFormJSON(jsonArray);
                }
                if(idToPostExecute == Connections.LEVEL){
                    CodeLanding.setLevelsFormJSON(jsonArray);
                }

                if(idToPostExecute == Connections.QUESTIONSOFSUBANDLEVEL){
                    QuestionTestActivity.setQuestionsFromJSON(jsonArray);
                }

                if(idToPostExecute == Connections.LOGIN){
                    LoginActivity.setUserFromJSON(jsonArray);
                }

                if(idToPostExecute == Connections.CREATION){
                    SingUpActivity.goToLoginScreen(true);
                }

                if(idToPostExecute == Connections.UPDATE_LEVEL_FINISHED){
                    Log.d(TAG,Connections.UPDATE_LEVEL_FINISHED+" data correctly posted");
                }

            } catch (JSONException e) {
                //e.printStackTrace();
                System.out.println("ERROR EN JSON DE RESPUESTA");
                if(idToPostExecute == Connections.QUESTIONSOFSUBANDLEVEL){
                    QuestionTestActivity.setQuestionsFromJSON(null);
                }
                if(idToPostExecute == Connections.LOGIN){
                    System.out.println("Error on LOGIN");
                    LoginActivity.setUserFromJSON(null);
                }

                if(idToPostExecute == Connections.CREATION){
                    System.out.println("Error on CREATION");
                    SingUpActivity.goToLoginScreen(false);
                }

                if(idToPostExecute == Connections.UPDATE_LEVEL_FINISHED){
                    Log.d(TAG, Connections.UPDATE_LEVEL_FINISHED + " data ERROR WHEN posted");
                }

            }
        }
    }

}
