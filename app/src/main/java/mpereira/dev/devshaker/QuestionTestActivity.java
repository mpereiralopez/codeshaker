package mpereira.dev.devshaker;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import mpereira.dev.devshaker.Utils.MenuUtils;
import mpereira.dev.devshaker.connection.Connections;
import mpereira.dev.devshaker.level.LevelActivity;
import mpereira.dev.devshaker.level.LevelCompleted;
import mpereira.dev.devshaker.persistance.DevShackerDbHelper;
import mpereira.dev.devshaker.persistance.Level;
import mpereira.dev.devshaker.persistance.Question;
import mpereira.dev.devshaker.persistance.Subcategory;


public class QuestionTestActivity extends Activity {

    private final String TAG = "QuestionTestActivity";
    private static Level levelSelected;
    private static long subcategoryId;
    private static Activity act;

    private static ProgressDialog progress;
    private static ProgressBar mProgress;

    private RelativeLayout questionLayout;
    private Subcategory subcategory;

    private static Question actualQuestion;

    private int index;

    private static TextView questionText;
    private static ImageView imageQuestion;

    private static LinkedList<Question> questions = new LinkedList<Question>();

    private static Button bA1;
    private static Button bA2;
    private static Button bA3;
    private static Button bA4;
    private static boolean isfromtutorail;

    private List<Boolean> listAnswerStatus = new LinkedList<Boolean>();

    private static DevShackerDbHelper dbhelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_test);
        act = this;

        isfromtutorail = false;
        
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            isfromtutorail = extras.getBoolean("IS_FROM_TUTORIAL");
        }

        questionLayout = (RelativeLayout) findViewById(R.id.questionLayout);
        questionLayout.setOnTouchListener(new RelativeLayoutTouchListener(this));

        mProgress = (ProgressBar) findViewById(R.id.progressBar);
        imageQuestion = (ImageView) findViewById(R.id.imageQuestion);
        this.index = 0;


        if (isfromtutorail) {
            actualQuestion = new Question(0, 0, "Which one of this operator is used to allocate memory?", null, "memory", "delete", "new", "allocate", "3", 0);
            Question q2 = new Question(0, 0, "Which language does not need to be compiled?", null, "Javascript", "C", "C++", "Fortran", "1", 0);
            Question q3 = new Question(0, 0, "What is the output of the program?", "@drawable/q3img", "print() is called 10 times", "x = 10 y=10", "; after if(x!=y) produces error", "No output",
                    "3", 0);
            questions.add(actualQuestion);
            questions.add(q3);
            questions.add(q2);

            setQuestionInfo(questions.getFirst(), index, this);
        } else {

            //Not from tutorial --> Could be new or loaded from DDBB
            dbhelper = new DevShackerDbHelper(this);
            SQLiteDatabase db = dbhelper.getReadableDatabase();
            this.levelSelected = (Level) getIntent().getSerializableExtra("Level");
            subcategory = (Subcategory) getIntent().getSerializableExtra("Subcategory");
            subcategoryId = subcategory.getId();

            int totalLevels = extras.getInt("totalLevels");

            Log.d(TAG, "LevelSelected id: " + levelSelected.getID());
            String selectQuery = "SELECT * FROM " + Question.QuestionEntry.TABLE_NAME + " WHERE " + Question.QuestionEntry.COLUMN_NAME_SUBCATEGORY_ID + "=" + levelSelected.getSubCategory()
                    + " AND " + Question.QuestionEntry.COLUMN_NAME_DIFFICULTY + "= " + levelSelected.getName() + ";";
            System.out.println(selectQuery);
            Cursor cursor = db.rawQuery(selectQuery, null);
            System.out.println(cursor.getCount());
            if (cursor.getCount() == 0) {
                progress = ProgressDialog.show(this, "Downloading",
                        "Downloading questions of test from Server, please wait", true);

                Log.d(TAG, "No questions for level, need to download");

                Connections connections = new Connections();
                Log.d(TAG, String.valueOf(subcategory.getId()) + " " + String.valueOf(levelSelected) + " " + String.valueOf(totalLevels));

                String[] params = {subcategory.getId() + "", levelSelected.getName() + "", totalLevels + ""};
                connections.callToWS(Connections.QUESTIONSOFSUBANDLEVEL, params,0);
            } else {
                Log.d(TAG, "There are questions on DDBB only to charge it");
                Log.d(TAG, subcategoryId + " " + levelSelected + " " + levelSelected.getNumberOfQuestions());
                questions = Question.getListQuestionBySubAndLevelFromDB(subcategoryId, Long.parseLong(levelSelected.getName()), levelSelected.getNumberOfQuestions(), db);

                setQuestionInfo(questions.getFirst(), index, this);
            }


        }


    }

    public static void setQuestionInfo(Question q, int index, Activity a) {
        actualQuestion = q;
        questionText = (TextView) a.findViewById(R.id.txtQuestion);

        Log.d("QuestionTestActivity", "RespuestaCorrecta: " + actualQuestion.getRightAnswers());

        bA1 = (Button) a.findViewById(R.id.buttonA1);
        bA2 = (Button) a.findViewById(R.id.buttonA2);
        bA3 = (Button) a.findViewById(R.id.buttonA3);
        bA4 = (Button) a.findViewById(R.id.buttonA4);

        questionText.setMovementMethod(new ScrollingMovementMethod());
        questionText.setText(q.getTextQuestion());

        if (q.getImageQuestion() != null && q.getImageQuestion().trim().length() != 0) {
            Uri imageUri = null;
            if (isfromtutorail) {
                imageUri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" +
                        a.getResources().getResourcePackageName(R.drawable.q3img) + '/' +
                        a.getResources().getResourceTypeName(R.drawable.q3img) + '/' +
                        a.getResources().getResourceEntryName(R.drawable.q3img));
            } else {

                Log.d("ImageURI", "ImageURI: " + q.getImageQuestion());

                imageUri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" +
                        a.getResources().getResourcePackageName(R.drawable.q3img) + '/' +
                        a.getResources().getResourceTypeName(R.drawable.q3img) + '/' +
                        a.getResources().getResourceEntryName(R.drawable.q3img));
            }
            System.out.println(imageUri.toString());
            imageQuestion.setImageURI(imageUri);
            imageQuestion.setVisibility(View.VISIBLE);
            questionText.setGravity(Gravity.NO_GRAVITY);
        } else {
            imageQuestion.setVisibility(View.INVISIBLE);
            questionText.setGravity(Gravity.CENTER);
            questionText.setGravity(Gravity.CENTER_VERTICAL);

        }

        bA1.setText(q.getAnswer1());
        bA2.setText(q.getAnswer2());
        bA3.setText(q.getAnswer3());
        bA4.setText(q.getAnswer4());

        mProgress.setProgress((index * 100) / questions.size());

        bA1.setActivated(false);
        bA1.setBackgroundResource(R.drawable.background);

        bA2.setActivated(false);
        bA2.setBackgroundResource(R.drawable.background);

        bA3.setActivated(false);
        bA3.setBackgroundResource(R.drawable.background);

        bA4.setActivated(false);
        bA4.setBackgroundResource(R.drawable.background);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_question_test, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void setFirstCorrect(View view) {

        if (bA1.isActivated()) {
            bA1.setActivated(false);
            bA1.setBackgroundResource(R.drawable.background);
        } else {
            bA1.setActivated(true);
            bA1.setBackgroundResource(R.drawable.selected_answer_bgn);
        }

    }

    public void setSecondCorrect(View view) {
        if (bA2.isActivated()) {
            bA2.setActivated(false);
            bA2.setBackgroundResource(R.drawable.background);
        } else {
            bA2.setActivated(true);
            bA2.setBackgroundResource(R.drawable.selected_answer_bgn);
        }
    }

    public void setThirdCorrect(View view) {
        if (bA3.isActivated()) {
            bA3.setActivated(false);
            bA3.setBackgroundResource(R.drawable.background);
        } else {
            bA3.setActivated(true);
            bA3.setBackgroundResource(R.drawable.selected_answer_bgn);
        }
        ;
    }

    public void setFourthCorrect(View view) {
        if (bA4.isActivated()) {
            bA4.setActivated(false);
            bA4.setBackgroundResource(R.drawable.background);
        } else {
            bA4.setActivated(true);
            bA4.setBackgroundResource(R.drawable.selected_answer_bgn);
        }
    }


    private class RelativeLayoutTouchListener implements View.OnTouchListener {

        static final String logTag = "ActivitySwipeDetector";
        private Activity activity;
        static final int MIN_DISTANCE = 100;// TODO change this runtime based on screen resolution. for 1920x1080 is to small the 100 distance
        private float downX, downY, upX, upY;

        // private MainActivity mMainActivity;

        public RelativeLayoutTouchListener(QuestionTestActivity mainActivity) {
            activity = mainActivity;
        }

        public void onRightToLeftSwipe() {
            Log.i(logTag, "Corregimos");
            if (!bA1.isActivated() && !bA2.isActivated() && !bA3.isActivated() && !bA4.isActivated()) {
                Log.i("No response", "No hay respuesta que corregir no hacemos nada");
            } else {
                String[] correctAnswers;
                if (actualQuestion.getRightAnswers().contains(",")) {
                    correctAnswers = actualQuestion.getRightAnswers().split(",");
                } else {
                    correctAnswers = new String[1];
                    correctAnswers[0] = actualQuestion.getRightAnswers();
                }

                List<String> selectedAnswers = new LinkedList<String>();
                //int counter = correctAnswers.length;

                if (bA1.isActivated()) {
                    selectedAnswers.add("1");
                }

                if (bA2.isActivated()) {
                    selectedAnswers.add("2");
                }

                if (bA3.isActivated()) {
                    selectedAnswers.add("3");
                }

                if (bA4.isActivated()) {
                    selectedAnswers.add("4");
                }

                //Now --> Check it
                boolean istestFinal = false;
                System.out.println(selectedAnswers.size() + " " + correctAnswers.length);
                if (selectedAnswers.size() == correctAnswers.length) {
                    int counter = 0;
                    for (int i = 0; i < correctAnswers.length; i++) {
                        if (selectedAnswers.contains(correctAnswers[i])) {
                            counter++;
                        }
                    }

                    if (counter == correctAnswers.length) {
                        //Todas respuestas correctas
                        if (index + 1 < questions.size()) {
                            listAnswerStatus.add(index, true);
                            index = index + 1;
                            actualQuestion = questions.get(index);
                            setQuestionInfo(actualQuestion, index, activity);
                        } else {
                            istestFinal = true;
                            listAnswerStatus.add(index, true);

                        }

                    } else {
                        Log.i("Response error", "La respuesta a la pregunta no es correcta");
                        if (index + 1 < questions.size()) {
                            listAnswerStatus.add(index, false);
                            index = index + 1;
                            actualQuestion = questions.get(index);
                            setQuestionInfo(actualQuestion, index, activity);
                        } else {
                            istestFinal = true;
                            listAnswerStatus.add(index, false);

                        }
                    }
                } else {
                    Log.i("Response error", "La respuesta a la pregunta no es correcta");
                    if (index + 1 < questions.size()) {
                        listAnswerStatus.add(index, false);
                        index = index + 1;
                        actualQuestion = questions.get(index);
                        setQuestionInfo(actualQuestion, index, activity);
                    } else {
                        istestFinal = true;
                        listAnswerStatus.add(index, false);

                    }

                }


                if (istestFinal) {
                    Log.i("FINAL DE TEST", "FINAL DEL TEST");
                    Iterator<Boolean> iterator = listAnswerStatus.iterator();
                    boolean listArray[] = new boolean[listAnswerStatus.size()];
                    int i = 0;
                    while (iterator.hasNext()) {
                        boolean value = iterator.next();
                        listArray[i] = value;
                        Log.i("Respuesta Correcta?", new Boolean(value).toString());
                        i = i + 1;
                    }

                    Intent mainIntent = new Intent(QuestionTestActivity.this, LevelCompleted.class);
                    mainIntent.putExtra("LIST_ANSWERS", listArray);
                    mainIntent.putExtra("isFromTutorial", isfromtutorail);

                    String header = new String();
                    if (isfromtutorail) {
                        header = "Tutorial";

                    } else {
                        header = subcategory.getName() + " - Level " + levelSelected.getName();
                    }
                    mainIntent.putExtra("LevelCompletedHeader", header);

                    if (!isfromtutorail) {
                        mainIntent.putExtra("lvlcompleted", levelSelected.getName());
                        mainIntent.putExtra("subcategory", subcategory);
                        mainIntent.putExtra("Level", levelSelected);
                    }


                    QuestionTestActivity.this.startActivity(mainIntent);
                    QuestionTestActivity.this.finish();

                }

            }
            //Toast.makeText(activity, "RightToLeftSwipe", Toast.LENGTH_SHORT).show();
            // activity.doSomething();

        }

        /*public void onLeftToRightSwipe() {
            Log.i(logTag, "LeftToRightSwipe!");
            Toast.makeText(activity, "LeftToRightSwipe", Toast.LENGTH_SHORT).show();
            // activity.doSomething();
        }

        public void onTopToBottomSwipe() {
            Log.i(logTag, "onTopToBottomSwipe!");
            Toast.makeText(activity, "onTopToBottomSwipe", Toast.LENGTH_SHORT).show();
            // activity.doSomething();
        }

        public void onBottomToTopSwipe() {
            Log.i(logTag, "onBottomToTopSwipe!");
            Toast.makeText(activity, "onBottomToTopSwipe", Toast.LENGTH_SHORT).show();
            // activity.doSomething();
        }*/

        public boolean onTouch(View v, MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN: {
                    downX = event.getX();
                    downY = event.getY();
                    return true;
                }
                case MotionEvent.ACTION_UP: {
                    upX = event.getX();
                    upY = event.getY();

                    float deltaX = downX - upX;
                    float deltaY = downY - upY;

                    // swipe horizontal?
                    if (Math.abs(deltaX) > MIN_DISTANCE) {
                        // left or right
                        /*if (deltaX < 0) {
                            this.onLeftToRightSwipe();
                            return true;
                        }*/
                        if (deltaX > 0) {
                            this.onRightToLeftSwipe();
                            return true;
                        }
                    } else {
                        Log.i(logTag, "Swipe was only " + Math.abs(deltaX) + " long horizontally, need at least " + MIN_DISTANCE);
                        // return false; // We don't consume the event
                    }

                    // swipe vertical?
                    if (Math.abs(deltaY) > MIN_DISTANCE) {
                        // top or down
                        /*if (deltaY < 0) {
                            this.onTopToBottomSwipe();
                            return true;
                        }
                        if (deltaY > 0) {
                            this.onBottomToTopSwipe();
                            return true;
                        }*/
                    } else {
                        Log.i(logTag, "Swipe was only " + Math.abs(deltaX) + " long vertically, need at least " + MIN_DISTANCE);
                        // return false; // We don't consume the event
                    }

                    return false; // no swipe horizontally and no swipe vertically
                }// case MotionEvent.ACTION_UP:
            }
            return false;
        }

    }


    public static void setQuestionsFromJSON(JSONArray jsonArray) {
        Gson gson = new Gson();
        SQLiteDatabase db = dbhelper.getWritableDatabase();
        Question question = null;


        if (jsonArray != null) {
            for (int i = 0; i < jsonArray.length(); i++) {

                try {
                    question = gson.fromJson(jsonArray.getJSONObject(i).toString(), Question.class);
                    Question.saveQuestionToDDBB(question, db);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            progress.dismiss();

            questions = Question.getListQuestionBySubAndLevelFromDB(subcategoryId, Long.parseLong(levelSelected.getName()), levelSelected.getNumberOfQuestions(), db);

            setQuestionInfo(questions.getFirst(), 0, act);


        } else {
            progress.dismiss();
            Context context = act.getApplicationContext();
            CharSequence text = "No test for this level in Server!";
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(context, text, duration);
            toast.show();

            Intent mainIntent = new Intent(act,MainMenu.class);
            act.startActivity(mainIntent);
            act.finish();

        }

    }

}

