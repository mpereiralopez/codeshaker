package mpereira.dev.devshaker.Utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.HashMap;

import mpereira.dev.devshaker.R;

/**
 * Created by mpereira on 11/4/15.
 */
public class Utils {

    public static final int answerStatusRadious = 60;
    public static final int codeLanguageRadious = 80;

    public static final String LEVEL_COMMPLETED_KEY = "levelCompleted";
    public static final String CODE_LANDING_KEY = "codeLanding";



    public static TextView createResponseCircle (Context context,Resources resources,HashMap<String,Object> mapa, String text,int h){

        float ht_px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, h, resources.getDisplayMetrics());
        TextView cloned = new TextView(context);
        cloned.setGravity(Gravity.CENTER);
        cloned.setTextColor(Color.WHITE);
        cloned.setTextSize(18);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(Math.round(ht_px),Math.round(ht_px));
        params.setMargins(15, 0, 5, 0); //substitute parameters for left, top, right, bottom
        cloned.setLayoutParams(params);
        cloned.setText(text);


        if(mapa.containsKey(LEVEL_COMMPLETED_KEY)){
            boolean correct = ((Boolean) mapa.get(LEVEL_COMMPLETED_KEY)).booleanValue();
            if (correct) {
                cloned.setBackgroundDrawable(resources.getDrawable(R.drawable.circle_green));
            } else{
                cloned.setBackgroundDrawable(resources.getDrawable(R.drawable.circle_red));
            }
        }

        if(mapa.containsKey(CODE_LANDING_KEY)){
            cloned.setBackgroundDrawable(resources.getDrawable(R.drawable.circle_code));
        }
        return cloned;
    }
}
