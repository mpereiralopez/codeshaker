package mpereira.dev.devshaker.Utils;

import android.app.Activity;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import mpereira.dev.devshaker.MyAdapter;
import mpereira.dev.devshaker.R;
import mpereira.dev.devshaker.persistance.DevShackerDbHelper;
import mpereira.dev.devshaker.persistance.User;

/**
 * Created by je10413 on 05/08/2015.
 */
public class MenuUtils {

    private String TITLES[] =  {"Home","Profile","Badge","Achievements","Compagnies","Friends","Settings"};// load titles from strings.xml

    private int ICONS[] = {R.drawable.home_menu,R.drawable.profile,R.drawable.badges,R.drawable.achievements,
            R.drawable.compagnies, R.drawable.friends, R.drawable.settings};


    private Toolbar toolbar;                              // Declaring the Toolbar Object

    RecyclerView mRecyclerView;                           // Declaring RecyclerView
    RecyclerView.Adapter mAdapter;                        // Declaring Adapter For Recycler View
    RecyclerView.LayoutManager mLayoutManager;            // Declaring Layout Manager as a linear layout manager
    DrawerLayout Drawer;                                  // Declaring DrawerLayout

    ActionBarDrawerToggle mDrawerToggle;                  // Declaring Action Bar Drawer Toggle


    private Context context;


    public MenuUtils(){
    }


    public void setLeftMenu(Context ctx, ActionBarActivity act) {
        toolbar = (Toolbar) act.findViewById(R.id.tool_bar);
        toolbar.setTitleTextColor(0xFFFFFFFF);
        act.setSupportActionBar(toolbar);
        mRecyclerView = (RecyclerView) act.findViewById(R.id.RecyclerView); // Assigning the RecyclerView Object to the xml View

        mRecyclerView.setHasFixedSize(true);                            // Letting the system know that the list objects are of fixed size
        DevShackerDbHelper dbhelper = new DevShackerDbHelper(ctx);
        SQLiteDatabase db =  dbhelper.getReadableDatabase();
        User user = User.getUserFromDDBB(db);
        String userName = user.getDisplayName();


        Bitmap bm = null;
        if(user.getImg().length()!=0){

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(user.getImg(), options);
            final int REQUIRED_SIZE = 200;
            int scale = 1;
            while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                    && options.outHeight / scale / 2 >= REQUIRED_SIZE)
                scale *= 2;
            options.inSampleSize = scale;
            options.inJustDecodeBounds = false;
            bm = BitmapFactory.decodeFile(user.getImg(), options);
        }



        mAdapter = new MyAdapter(TITLES, ICONS, userName, bm,act,ctx);       // Creating the Adapter of MyAdapter class(which we are going to see in a bit)
        // And passing the titles,icons,header view name, header view email,
        // and header view profile picture

        mRecyclerView.setAdapter(mAdapter);                              // Setting the adapter to RecyclerView

        mLayoutManager = new LinearLayoutManager(ctx);                 // Creating a layout Manager

        mRecyclerView.setLayoutManager(mLayoutManager);                 // Setting the layout Manager


        Drawer = (DrawerLayout) act.findViewById(R.id.drawer_layout);        // Drawer object Assigned to the view
        mDrawerToggle = new ActionBarDrawerToggle(act, Drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {


            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                // code here will execute once the drawer is opened( As I dont want anything happened whe drawer is
                // open I am not going to put anything here)
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                // Code here will execute once drawer is closed
            }



        }; // Drawer Toggle Object Made
        Drawer.setDrawerListener(mDrawerToggle); // Drawer Listener set to the Drawer toggle
        mDrawerToggle.syncState();               // Finally we set the drawer toggle sync State

    }
}
