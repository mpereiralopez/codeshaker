package mpereira.dev.devshaker.Utils;

/**
 * Created by mpereira on 7/8/15.
 */
public class ExperienceCalculator {

    private static final int INDEX_10 = 10;
    private static final int INDEX_15 = 15;
    private static final int INDEX_30 = 30;
    private static final int INDEX_45 = 45;
    private static final int INDEX_60 = 60;
    private static final int INDEX_75 = 75;


    private static final int PERCENTAGE_20 = 20;
    private static final int PERCENTAGE_40 = 40;
    private static final int PERCENTAGE_60 = 60;
    private static final int PERCENTAGE_80 = 80;
    private static final int PERCENTAGE_100 = 100;


    private static final int LIMIT_LEVEL_1 = 30;
    private static final int LIMIT_LEVEL_2 = 52;
    private static final int LIMIT_LEVEL_3 = 92;
    private static final int LIMIT_LEVEL_4 = 160;
    private static final int LIMIT_LEVEL_5 = 281;
    private static final int LIMIT_LEVEL_6 = 492;
    private static final int LIMIT_LEVEL_7 = 860;
    private static final int LIMIT_LEVEL_8 = 1507;
    private static final int LIMIT_LEVEL_9 = 2640;
    private static final int LIMIT_LEVEL_10 = 4620;
    private static final int LIMIT_LEVEL_11 = 8080;
    private static final int LIMIT_LEVEL_12 = 14140;
    private static final int LIMIT_LEVEL_13 = 24750;
    private static final int LIMIT_LEVEL_14 = 37125;
    private static final int LIMIT_LEVEL_15 = 55680;



    public static int getExperienceByLevelAndPercentage(int level, int percentage){
        int xp = 0;
        if(level==0)level=1;
        if(percentage<= PERCENTAGE_20){
            xp = INDEX_10*level;
        }else if(percentage > PERCENTAGE_20 && percentage <= PERCENTAGE_40 ){
            xp = INDEX_15*level;

        }else if(percentage > PERCENTAGE_40 && percentage <= PERCENTAGE_60){
            xp = INDEX_30*level;

        }else if(percentage > PERCENTAGE_60 && percentage <= PERCENTAGE_80){
            xp = INDEX_45*level;

        }else if(percentage > PERCENTAGE_80 && percentage < PERCENTAGE_100){
            xp = INDEX_60*level;

        }else if(percentage==PERCENTAGE_100){
            xp = INDEX_75*level;
        }

        return xp;
    }

    public static int[] getLevelOfProgressionOfUser (int userXp){
        int level = 0;
        int limit = LIMIT_LEVEL_1;
        if(userXp<=LIMIT_LEVEL_1){
            level = 0;
            limit = LIMIT_LEVEL_1;
        }else if(userXp > LIMIT_LEVEL_1 && userXp <= LIMIT_LEVEL_2 ){
            level = 1;
            limit = LIMIT_LEVEL_2;
        }else if(userXp > LIMIT_LEVEL_2 && userXp <= LIMIT_LEVEL_3){
            level = 2;
            limit = LIMIT_LEVEL_3;
        }else if(userXp > LIMIT_LEVEL_3 && userXp <= LIMIT_LEVEL_4){
            level = 3;
            limit = LIMIT_LEVEL_4;
        }else if(userXp > LIMIT_LEVEL_4 && userXp < LIMIT_LEVEL_5){
            level = 4;
            limit = LIMIT_LEVEL_5;
        }else if(userXp > LIMIT_LEVEL_5 && userXp < LIMIT_LEVEL_6){
            level = 5;
            limit = LIMIT_LEVEL_6;
        }else if(userXp > LIMIT_LEVEL_6 && userXp < LIMIT_LEVEL_7){
            level = 6;
            limit = LIMIT_LEVEL_7;
        }else if(userXp > LIMIT_LEVEL_7 && userXp < LIMIT_LEVEL_8){
            level = 7;
            limit = LIMIT_LEVEL_8;
        }else if(userXp > LIMIT_LEVEL_8 && userXp < LIMIT_LEVEL_9){
            level = 8;
            limit = LIMIT_LEVEL_9;
        }else if(userXp > LIMIT_LEVEL_9 && userXp < LIMIT_LEVEL_10){
            level = 9;
            limit = LIMIT_LEVEL_10;
        }else if(userXp > LIMIT_LEVEL_10 && userXp < LIMIT_LEVEL_11){
            level = 10;
            limit = LIMIT_LEVEL_11;
        }else if(userXp > LIMIT_LEVEL_11 && userXp < LIMIT_LEVEL_12){
            level = 11;
            limit = LIMIT_LEVEL_12;
        }else if(userXp > LIMIT_LEVEL_12 && userXp < LIMIT_LEVEL_13){
            level = 12;
            limit = LIMIT_LEVEL_13;
        }else if(userXp > LIMIT_LEVEL_13 && userXp < LIMIT_LEVEL_14){
            level = 13;
            limit = LIMIT_LEVEL_14;
        }else if(userXp > LIMIT_LEVEL_14 && userXp < LIMIT_LEVEL_15){
            level = 14;
            limit = LIMIT_LEVEL_15;
        }else if(userXp > LIMIT_LEVEL_15){
            level = 15;
            limit = LIMIT_LEVEL_15;
        }

        int [] returnValues = {level,limit};
        return returnValues;
    }

}
