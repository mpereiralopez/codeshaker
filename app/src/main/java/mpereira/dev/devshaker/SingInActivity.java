package mpereira.dev.devshaker;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class SingInActivity extends Activity {

    private Button singInBtn;
    private Button loginWithMailBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sing_in);

        singInBtn = (Button)findViewById(R.id.singinbtn);
        loginWithMailBtn = (Button)findViewById(R.id.registerByEmailbtn);

        singInBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Metodo de login");
                Intent mainIntent = new Intent(SingInActivity.this,LoginActivity.class);
                mainIntent.putExtra("isNewAccount",false);
                SingInActivity.this.startActivity(mainIntent);
            }
        });

        loginWithMailBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Metodo de alta de usuario por mail");
                Intent mainIntent = new Intent(SingInActivity.this,SingUpActivity.class);
                mainIntent.putExtra("isNewAccount",true);
                SingInActivity.this.startActivity(mainIntent);
            }
        });

    }


}
