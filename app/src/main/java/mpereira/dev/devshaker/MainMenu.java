package mpereira.dev.devshaker;

import android.app.Activity;
import android.content.Intent;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import mpereira.dev.devshaker.Utils.MenuUtils;
import mpereira.dev.devshaker.level.CodeLanding;
import mpereira.dev.devshaker.persistance.Category;
import mpereira.dev.devshaker.persistance.DevShackerDbHelper;
import mpereira.dev.devshaker.persistance.Subcategory;

/**
 * Created by mpereira on 2/5/15.
 */
public class MainMenu extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_landing);

        MenuUtils utils = new MenuUtils();
        ActionBarActivity act = this;
        utils.setLeftMenu(this, act);


        DevShackerDbHelper dbhelper = new DevShackerDbHelper(this);

        SQLiteDatabase db =  dbhelper.getReadableDatabase();

        String selectQuery = "SELECT * FROM " + Category.CategoryEntry.TABLE_NAME+";";
        Cursor cursor = db.rawQuery(selectQuery, null);
        System.out.println(cursor.getCount());
        List<Category> categoryList = new ArrayList<Category>();
        Category category= null;
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                category = new Category(
                        Long.parseLong(cursor.getString(0)),
                        cursor.getString(1)
                );

                categoryList.add(category);
            } while (cursor.moveToNext());
        }

        Iterator<Category> it = categoryList.iterator();

        while(it.hasNext()){
            Category cat = it.next();
            System.out.println(cat.getName());
        }


    }

    public void goToCodeLanding(View view){
        //System.out.println("Button pressed");
        Intent mainIntent = new Intent(MainMenu.this,CodeLanding.class);
        MainMenu.this.startActivity(mainIntent);
        //MainMenu.this.finish();
    }

}
