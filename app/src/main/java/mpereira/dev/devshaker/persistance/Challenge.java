package mpereira.dev.devshaker.persistance;

import android.provider.BaseColumns;

import java.util.List;

/**
 * Created by mpereira on 14/3/15.
 */
public class Challenge {

    private long id;
    private long company_id;
    private String name;
    private int nbLevel;
    private int nbQuestions;
    private String description;
    private long startDate;
    private long endDate;

    private List<Question> questionsOfChallenge;


    public Challenge(long id, long company_id, String name, int nbLevel, int nbQuestions, String description, long startDate, long endDate, List<Question> questionsOfChallenge) {
        this.id = id;
        this.company_id = company_id;
        this.name = name;
        this.nbLevel = nbLevel;
        this.nbQuestions = nbQuestions;
        this.description = description;
        this.startDate = startDate;
        this.endDate = endDate;
        this.questionsOfChallenge = questionsOfChallenge;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getCompany_id() {
        return company_id;
    }

    public void setCompany_id(long company_id) {
        this.company_id = company_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNbLevel() {
        return nbLevel;
    }

    public void setNbLevel(int nbLevel) {
        this.nbLevel = nbLevel;
    }

    public int getNbQuestions() {
        return nbQuestions;
    }

    public void setNbQuestions(int nbQuestions) {
        this.nbQuestions = nbQuestions;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getStartDate() {
        return startDate;
    }

    public void setStartDate(long startDate) {
        this.startDate = startDate;
    }

    public long getEndDate() {
        return endDate;
    }

    public void setEndDate(long endDate) {
        this.endDate = endDate;
    }

    public List<Question> getQuestionsOfChallenge() {
        return questionsOfChallenge;
    }

    public void setQuestionsOfChallenge(List<Question> questionsOfChallenge) {
        this.questionsOfChallenge = questionsOfChallenge;
    }

    public static abstract class ChallengeEntry implements BaseColumns {
        public static final String TABLE_NAME = "challenge";
        public static final String COLUMN_NAME_CHALLENGE_ID = "id";
        public static final String COLUMN_NAME_CHALLENGE_COMPANY_ID = "compay_id";
        public static final String COLUMN_NAME_CHALLENGE_NAME = "name";
        public static final String COLUMN_NAME_CHALLENGE_LEVEL = "level";
        public static final String COLUMN_NAME_CHALLENGE_NQUESTIONS = "number_questions";
        public static final String COLUMN_NAME_CHALLENGE_DESC = "description";
        public static final String COLUMN_NAME_CHALLENGE_STARTDATE = "start_date";
        public static final String COLUMN_NAME_CHALLENGE_ENDDATE = "end_date";
    }

    public static abstract class QuestionOfChallenge implements BaseColumns {
        public static final String TABLE_NAME = "question_of_challenge";
        public static final String COLUMN_NAME_CHALLENGE_ID = "challenge_id";
        public static final String COLUMN_NAME_CHALLENGE_COMPANY_ID = "question_id";

    }
}
