package mpereira.dev.devshaker.persistance;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by mpereira on 4/6/15.
 */
public class Subcategory implements Serializable {
    private static final long serialVersionUID = 1L;


    private long ID;
    private long category;
    private String name;


    public long getId() {
        return ID;
    }

    public void setId(long id) {
        this.ID = id;
    }

    public long getCategory() {
        return category;
    }

    public void setCategory(long category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Subcategory(long ID, long category,String name) {
        this.ID = ID;
        this.category = category;
        this.name = name;
    }

    public static long saveSubcategoryToDDBB(Subcategory subcategory, SQLiteDatabase db){
        ContentValues initialValues = new ContentValues();
        long returnValue=0;
        initialValues.put(Subcategory.SubcategoryEntry.COLUMN_NAME_SUBCATEGORY_ID, subcategory.getId());
        initialValues.put(Subcategory.SubcategoryEntry.COLUMN_NAME_CATEGORY_ID, subcategory.getCategory());
        initialValues.put(Subcategory.SubcategoryEntry.COLUMN_NAME_SUBCATEGORY_NAME, subcategory.getName());
        System.out.println(subcategory.getId()+ " "+ subcategory.getCategory()+" "+subcategory.getName());

        returnValue = db.insertWithOnConflict(Subcategory.SubcategoryEntry.TABLE_NAME,
                Subcategory.SubcategoryEntry.COLUMN_NAME_SUBCATEGORY_ID, initialValues, SQLiteDatabase.CONFLICT_REPLACE);
        System.out.println("ReturnValue: " + returnValue);

        Log.d("Splash", "Subcategoria " + subcategory.getName() + " anadida correctamente");
        return returnValue;
    }


    public static List<Subcategory> getListSubcategoryFromDB(long categoryId, SQLiteDatabase db){
        String selectQuery = "SELECT * FROM " + Subcategory.SubcategoryEntry.TABLE_NAME+" WHERE "+
                Subcategory.SubcategoryEntry.COLUMN_NAME_CATEGORY_ID+" = 1;";
        Cursor cursor = db.rawQuery(selectQuery, null);
        List<Subcategory> subcategoryList = new LinkedList<Subcategory>();
        Subcategory subcategory = null;
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                subcategory = new Subcategory(
                        Long.parseLong(cursor.getString(0)),
                        Long.parseLong(cursor.getString(1)),
                        cursor.getString(2)
                );

                subcategoryList.add(subcategory);
            } while (cursor.moveToNext());
        }

        return subcategoryList;
    }


    public static abstract class SubcategoryEntry implements BaseColumns {
        public static final String TABLE_NAME = "Subcategory";
        public static final String COLUMN_NAME_SUBCATEGORY_ID = "ID";
        public static final String COLUMN_NAME_CATEGORY_ID = "category";
        public static final String COLUMN_NAME_SUBCATEGORY_NAME = "name";
    }
}
