package mpereira.dev.devshaker.persistance;

import android.provider.BaseColumns;

/**
 * Created by mpereira on 4/6/15.
 */
public class Category {

    private long id;
    private String name;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Category(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public static abstract class CategoryEntry implements BaseColumns {
        public static final String TABLE_NAME = "category";
        public static final String COLUMN_NAME_CATEGORY_ID = "ID";
        public static final String COLUMN_NAME_CATEGORY_NAME = "name";
    }
}
