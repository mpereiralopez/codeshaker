package mpereira.dev.devshaker.persistance;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import android.util.Log;

import java.io.Serializable;

/**
 * Created by mpereira on 6/8/15.
 */
public class User implements Serializable {

    private static final long serialVersionUID = 1L;


    private int ID;
    private String displayName;
    private String name;
    private String email;
    private String password;
    private String gender;
    private String birthdate;
    private String country;
    private String img;
    private int xp;

    public User(int ID, String displayName, String name, String email, String password, String gender, String birthdate, String country, String img,int xp) {
        this.ID = ID;
        this.displayName = displayName;
        this.name = name;
        this.email = email;
        this.password = password;
        this.gender = gender;
        this.birthdate = birthdate;
        this.country = country;
        this.img = img;
        this.xp = xp;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public int getXp() {
        return xp;
    }

    public void setXp(int xp) {
        this.xp = xp;
    }

    public static long saveUserToDDBB(User u, SQLiteDatabase db){
        ContentValues initialValues = new ContentValues();
        long returnValue=0;


        initialValues.put(UserEntry.COLUMN_NAME_USER_ID, u.getID());
        initialValues.put(UserEntry.COLUMN_NAME_DISPLAY_NAME, u.getDisplayName());
        initialValues.put(UserEntry.COLUMN_NAME_NAME, u.getName());

        initialValues.put(UserEntry.COLUMN_NAME_EMAIL, u.getEmail());
        initialValues.put(UserEntry.COLUMN_NAME_PASSWORD, u.getPassword());
        initialValues.put(UserEntry.COLUMN_NAME_GENDER, u.getGender());
        initialValues.put(UserEntry.COLUMN_NAME_BIRTHDATE, u.getBirthdate());
        initialValues.put(UserEntry.COLUMN_NAME_COUNTRY, u.getCountry());
        initialValues.put(UserEntry.COLUMN_NAME_IMG, u.getImg());


        returnValue = db.insert(User.UserEntry.TABLE_NAME,
                UserEntry.COLUMN_NAME_USER_ID, initialValues);



        System.out.println("ReturnValue: " + returnValue);

        Log.d("USER", "Usuario con nombre " + u.getName() + " anadido correctamente");
        return returnValue;
    }


    public static User getUserFromDDBB(SQLiteDatabase db){

        String selectQuery = "SELECT * FROM " + User.UserEntry.TABLE_NAME+" LIMIT 1;";
        System.out.println(selectQuery);
        Cursor cursor = db.rawQuery(selectQuery, null);
        User u = null;
        if (cursor.moveToFirst()) {
            int uxp = 0;

            if(cursor.getString(9)!=null){
                uxp = Integer.parseInt(cursor.getString(9));
            }
            u = new User(Integer.parseInt(cursor.getString(0)),
                    cursor.getString(1),cursor.getString(2),
                    cursor.getString(3),cursor.getString(4),
                    cursor.getString(5),cursor.getString(6),
                    cursor.getString(7),cursor.getString(8),uxp);
        }

        return u;
    }

    public static void updateUserProfilePic(int userId, String newUri, SQLiteDatabase db){
        Log.d("USER", "updateUserXp");
        //Cursor cursor = db.rawQuery(updateQuery, null);
        ContentValues valores = new ContentValues();
        valores.put(UserEntry.COLUMN_NAME_IMG, newUri);
        String whereClausule = UserEntry.COLUMN_NAME_USER_ID+"="+userId;
        int returnValue = db.update(UserEntry.TABLE_NAME, valores, whereClausule ,null);
        Log.d("USER",returnValue+"");
    }


    public static void updateUserXp(int userId ,int newXp,SQLiteDatabase db){


        Log.d("USER", "updateUserXp");
        //Cursor cursor = db.rawQuery(updateQuery, null);
        ContentValues valores = new ContentValues();
        valores.put(UserEntry.COLUMN_NAME_XP, newXp);
        String whereClausule = UserEntry.COLUMN_NAME_USER_ID+"="+userId;
        int returnValue = db.update(UserEntry.TABLE_NAME, valores, whereClausule ,null);
        Log.d("USER",returnValue+"");
    }

    public static abstract class UserEntry implements BaseColumns {
        public static final String TABLE_NAME = "Users";
        public static final String COLUMN_NAME_USER_ID = "ID";
        public static final String COLUMN_NAME_DISPLAY_NAME = "displayName";
        public static final String COLUMN_NAME_NAME = "name";
        public static final String COLUMN_NAME_EMAIL = "email";
        public static final String COLUMN_NAME_PASSWORD = "password";
        public static final String COLUMN_NAME_GENDER = "gender";
        public static final String COLUMN_NAME_BIRTHDATE = "birthdate";
        public static final String COLUMN_NAME_COUNTRY = "country";
        public static final String COLUMN_NAME_IMG = "img";
        public static final String COLUMN_NAME_XP = "xp";


    }
}
