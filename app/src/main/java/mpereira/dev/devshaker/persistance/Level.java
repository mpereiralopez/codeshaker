package mpereira.dev.devshaker.persistance;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import android.util.Log;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by je10413 on 11/06/2015.
 */
public class Level implements Serializable {

    private static final long serialVersionUID = 1L;


    private static final String TAG="LEVEL_POJO";

    private int ID;
    private String name;
    private long subCategory;
    private boolean isPassedByUser;
    private int numberOfQuestions;
    private int percentage;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(long subCategory) {
        this.subCategory = subCategory;
    }

    public boolean isPassedByUser() {
        return isPassedByUser;
    }

    public void setIsPassedByUser(boolean isPassedByUser) {
        this.isPassedByUser = isPassedByUser;
    }

    public int getNumberOfQuestions() {
        return numberOfQuestions;
    }

    public void setNumberOfQuestions(int numberOfQuestions) {
        this.numberOfQuestions = numberOfQuestions;
    }

    public int getPercentage() {
        return percentage;
    }

    public void setPercentage(int percentage) {
        this.percentage = percentage;
    }

    public Level(int ID, String name, long subCategory, boolean isPassedByUser, int numberOfQuestions, int percentage) {
        this.ID = ID;
        this.name = name;
        this.subCategory = subCategory;
        this.isPassedByUser = isPassedByUser;
        this.numberOfQuestions = numberOfQuestions;
        this.percentage = percentage;
    }

    public static List<Level> getTotalLevelOfSubcategory(long subcategoryId, SQLiteDatabase db){

        String selectQuery = "SELECT * FROM " + LevelEntry.TABLE_NAME+" WHERE "+
                Level.LevelEntry.COLUMN_NAME_SUBCATEGORY_ID+"="+subcategoryId+";";
        System.out.println(selectQuery);
        Cursor cursor = db.rawQuery(selectQuery, null);
        Level level = null;
        List<Level> list = new LinkedList<Level>();
        if (cursor.moveToFirst()) {
            do {
                //public Question(long ID, long ID_SubCat, String textQuestion, String imageQuestion, String answer1, String answer2, String answer3, String answer4, String rightAnswers, int difficulty)
                level = new Level(
                        Integer.parseInt(cursor.getString(0)),
                        cursor.getString(1),
                        Long.parseLong(cursor.getString(2)),
                        Boolean.parseBoolean(cursor.getString(3)),
                        Integer.parseInt(cursor.getString(4)),
                        Integer.parseInt(cursor.getString(5))
                );

                list.add(level);
            } while (cursor.moveToNext());
            return list;

        }else{
            return null;
        }
    }

    public static Level getLevelByLevelSubCatAndName(int subCategory, int levelName, SQLiteDatabase db){
        String selectQuery = "SELECT * FROM " + LevelEntry.TABLE_NAME+" WHERE "+
                LevelEntry.COLUMN_NAME_NAME+"="+levelName+" AND "+LevelEntry.COLUMN_NAME_SUBCATEGORY_ID+" = "+subCategory+";";
        System.out.println(selectQuery);
        Cursor cursor = db.rawQuery(selectQuery, null);

        Level level = null;

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            level = new Level(
                    Integer.parseInt(cursor.getString(0)),
                    cursor.getString(1),
                    Long.parseLong(cursor.getString(2)),
                    Boolean.parseBoolean(cursor.getString(3)),
                    Integer.parseInt(cursor.getString(4)),
                    Integer.parseInt(cursor.getString(5))
            );
            return level;

        }else{
            return null;
        }
    }

    public static int getTotalLevelsOfSubcategory (long subcategoryId, SQLiteDatabase db){
        int returnValue = 0;

        String selectQuery = "SELECT * FROM " + LevelEntry.TABLE_NAME+" WHERE "+
                Level.LevelEntry.COLUMN_NAME_SUBCATEGORY_ID+"="+subcategoryId+";";
        System.out.println(selectQuery);
        Cursor cursor = db.rawQuery(selectQuery, null);
        returnValue = cursor.getCount();
        return returnValue;
    }

    public static Level getLastLevelOfSubcategory(long subcategoryId,SQLiteDatabase db){
        String selectQuery = "SELECT * FROM " + LevelEntry.TABLE_NAME+" WHERE "+
                Level.LevelEntry.COLUMN_NAME_SUBCATEGORY_ID+"="+subcategoryId+" AND " +
                LevelEntry.COLUMN_NAME_ISPASSED+"=0 ORDER BY "+ LevelEntry.COLUMN_NAME_LEVEL+" ASC LIMIT 1";
        Cursor cursor = db.rawQuery(selectQuery, null);

        Level level = null;

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            level = new Level(
                    Integer.parseInt(cursor.getString(0)),
                    cursor.getString(1),
                    Long.parseLong(cursor.getString(2)),
                    Boolean.parseBoolean(cursor.getString(3)),
                    Integer.parseInt(cursor.getString(4)),
                    Integer.parseInt(cursor.getString(5))
            );
            return level;

        }else{
            return null;
        }

    }


    public static long saveLevelsToDDBB(Level level, SQLiteDatabase db){
        ContentValues initialValues = new ContentValues();
        long returnValue=0;
        initialValues.put(Level.LevelEntry.COLUMN_NAME_LEVEL, level.getID());
        initialValues.put(Level.LevelEntry.COLUMN_NAME_NAME, level.getName());
        initialValues.put(Level.LevelEntry.COLUMN_NAME_SUBCATEGORY_ID, level.getSubCategory());
        initialValues.put(Level.LevelEntry.COLUMN_NAME_NUMBER_OF_QUESTIONS, level.getNumberOfQuestions());
        initialValues.put(Level.LevelEntry.COLUMN_NAME_ISPASSED, false);
        initialValues.put(Level.LevelEntry.COLUMN_NAME_PERCENTAGE_PASSED,0);

        returnValue = db.insertWithOnConflict(Level.LevelEntry.TABLE_NAME,
                Level.LevelEntry.COLUMN_NAME_LEVEL, initialValues, SQLiteDatabase.CONFLICT_REPLACE);
        System.out.println("ReturnValue: " + returnValue);

        Log.d("Level", "Level " + level.getID() + " anadida correctamente");
        return returnValue;
    }

    public static void updateLevelStatusAndPercentage(int levelId,int percentage,SQLiteDatabase db){


        Log.d(TAG, "updateLevelStatus");
        //Cursor cursor = db.rawQuery(updateQuery, null);
        ContentValues valores = new ContentValues();
        valores.put(LevelEntry.COLUMN_NAME_ISPASSED, "true");
        valores.put(LevelEntry.COLUMN_NAME_PERCENTAGE_PASSED, percentage);
        String whereClausule = LevelEntry.COLUMN_NAME_LEVEL+"="+levelId;
        int returnValue = db.update(LevelEntry.TABLE_NAME, valores, whereClausule ,null);
        Log.d(TAG,returnValue+"");
        Log.d(TAG,percentage+"");
    }


    public static abstract class LevelEntry implements BaseColumns {
        public static final String TABLE_NAME = "Level";
        public static final String COLUMN_NAME_LEVEL = "ID";
        public static final String COLUMN_NAME_NAME = "name";
        public static final String COLUMN_NAME_SUBCATEGORY_ID= "subCategory";
        public static final String COLUMN_NAME_ISPASSED= "isPassedByUser";
        public static final String COLUMN_NAME_NUMBER_OF_QUESTIONS = "numberOfQuestions";
        public static final String COLUMN_NAME_PERCENTAGE_PASSED = "percentage";


    }
}
