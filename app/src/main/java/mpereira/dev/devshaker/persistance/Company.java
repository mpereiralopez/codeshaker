package mpereira.dev.devshaker.persistance;

import android.provider.BaseColumns;

/**
 * Created by mpereira on 14/3/15.
 */
public class Company {

    private long id;
    private String name;
    private String description;
    private String image_url;


    public Company(long id, String name, String description, String image_url) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.image_url = image_url;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public static abstract class CompanyEntry implements BaseColumns {
        public static final String TABLE_NAME = "company";
        public static final String COLUMN_NAME_COMPANY_ID = "id";
        public static final String COLUMN_NAME_COMPANY_NAME = "name";
        public static final String COLUMN_NAME_COMPANY_DESC = "description";
        public static final String COLUMN_NAME_COMPANY_IMAGE_URL = "image_url";
    }
}
