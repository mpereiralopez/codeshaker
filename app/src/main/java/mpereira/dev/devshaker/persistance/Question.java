package mpereira.dev.devshaker.persistance;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import android.util.Log;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by mpereira on 14/3/15.
 */
public class Question {

    private static final String TAG = "QUESTION_POJO";

    private long ID;
    private long ID_SubCat;
    private String textQuestion;
    private String imageQuestion;
    private String answer1;
    private String answer2;
    private String answer3;
    private String answer4;
    private String rightAnswers;
    private int difficulty;

    public Question(long ID, long ID_SubCat, String textQuestion, String imageQuestion, String answer1, String answer2, String answer3, String answer4, String rightAnswers, int difficulty) {
        this.ID = ID;
        this.ID_SubCat = ID_SubCat;
        this.textQuestion = textQuestion;
        this.imageQuestion = imageQuestion;
        this.answer1 = answer1;
        this.answer2 = answer2;
        this.answer3 = answer3;
        this.answer4 = answer4;
        this.rightAnswers = rightAnswers;
        this.difficulty = difficulty;
    }

    public long getID() {
        return ID;
    }

    public void setID(long ID) {
        this.ID = ID;
    }

    public long getID_SubCat() {
        return ID_SubCat;
    }

    public void setID_SubCat(long ID_SubCat) {
        this.ID_SubCat = ID_SubCat;
    }

    public String getTextQuestion() {
        return textQuestion;
    }

    public void setTextQuestion(String textQuestion) {
        this.textQuestion = textQuestion;
    }

    public String getImageQuestion() {
        return imageQuestion;
    }

    public void setImageQuestion(String imageQuestion) {
        this.imageQuestion = imageQuestion;
    }

    public String getAnswer1() {
        return answer1;
    }

    public void setAnswer1(String answer1) {
        this.answer1 = answer1;
    }

    public String getAnswer2() {
        return answer2;
    }

    public void setAnswer2(String answer2) {
        this.answer2 = answer2;
    }

    public String getAnswer3() {
        return answer3;
    }

    public void setAnswer3(String answer3) {
        this.answer3 = answer3;
    }

    public String getAnswer4() {
        return answer4;
    }

    public void setAnswer4(String answer4) {
        this.answer4 = answer4;
    }

    public String getRightAnswers() {
        return rightAnswers;
    }

    public void setRightAnswers(String rightAnswers) {
        this.rightAnswers = rightAnswers;
    }

    public int getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(int difficulty) {
        this.difficulty = difficulty;
    }

    public static LinkedList<Question> getListQuestionBySubAndLevelFromDB(long subcategoty,long level,int limit,SQLiteDatabase db){
        String selectQuery = "SELECT * FROM " + Question.QuestionEntry.TABLE_NAME+" WHERE "+
                QuestionEntry.COLUMN_NAME_SUBCATEGORY_ID+" = "+subcategoty+ " AND "+ QuestionEntry.COLUMN_NAME_DIFFICULTY+"="+level+" ORDER BY RANDOM() LIMIT "+limit+";";
        Log.d(TAG,selectQuery);
        Cursor cursor = db.rawQuery(selectQuery, null);
        LinkedList<Question> questionList = new LinkedList<Question>();
        Question question = null;
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                //public Question(long ID, long ID_SubCat, String textQuestion, String imageQuestion, String answer1, String answer2, String answer3, String answer4, String rightAnswers, int difficulty)
                question = new Question(
                        Long.parseLong(cursor.getString(0)),
                        Long.parseLong(cursor.getString(1)),
                        cursor.getString(2),
                        cursor.getString(3),
                        cursor.getString(4),
                        cursor.getString(5),
                        cursor.getString(6),
                        cursor.getString(7),
                        cursor.getString(8),
                        Integer.parseInt(cursor.getString(9))
                        );

                questionList.add(question);
            } while (cursor.moveToNext());
        }

        return questionList;
    }

    public static long saveQuestionToDDBB(Question question, SQLiteDatabase db){
        ContentValues initialValues = new ContentValues();
        long returnValue=0;
        initialValues.put(Question.QuestionEntry.COLUMN_NAME_QUESTION_ID, question.getID());
        initialValues.put(Question.QuestionEntry.COLUMN_NAME_SUBCATEGORY_ID, question.getID_SubCat());
        initialValues.put(Question.QuestionEntry.COLUMN_NAME_QUESTION_TEXT, question.getTextQuestion());
        initialValues.put(Question.QuestionEntry.COLUMN_NAME_QUESTION_IMAGE_URL, question.getImageQuestion());
        initialValues.put(Question.QuestionEntry.COLUMN_NAME_ANSWER1, question.getAnswer1());
        initialValues.put(Question.QuestionEntry.COLUMN_NAME_ANSWER2, question.getAnswer2());
        initialValues.put(Question.QuestionEntry.COLUMN_NAME_ANSWER3, question.getAnswer3());
        initialValues.put(Question.QuestionEntry.COLUMN_NAME_ANSWER4, question.getAnswer4());
        initialValues.put(Question.QuestionEntry.COLUMN_NAME_RIGHT_ANSWERS, question.getRightAnswers());
        initialValues.put(Question.QuestionEntry.COLUMN_NAME_DIFFICULTY, question.getDifficulty());


        returnValue = db.insertWithOnConflict(Question.QuestionEntry.TABLE_NAME,
                QuestionEntry.COLUMN_NAME_QUESTION_ID, initialValues, SQLiteDatabase.CONFLICT_REPLACE);

        Log.d(TAG, "Question " + question.getID() + " anadida correctamente");
        return returnValue;
    }



    public static abstract class QuestionEntry implements BaseColumns {
        public static final String TABLE_NAME = "questions";
        public static final String COLUMN_NAME_QUESTION_ID = "ID";
        public static final String COLUMN_NAME_SUBCATEGORY_ID = "ID_SubCat";
        public static final String COLUMN_NAME_QUESTION_TEXT = "textQuestion";
        public static final String COLUMN_NAME_QUESTION_IMAGE_URL = "imageQuestion";
        public static final String COLUMN_NAME_ANSWER1 = "answer1";
        public static final String COLUMN_NAME_ANSWER2 = "answer2";
        public static final String COLUMN_NAME_ANSWER3 = "answer3";
        public static final String COLUMN_NAME_ANSWER4 = "answer4";
        public static final String COLUMN_NAME_RIGHT_ANSWERS = "rightAnswers";
        public static final String COLUMN_NAME_DIFFICULTY = "difficulty";
    }
}
