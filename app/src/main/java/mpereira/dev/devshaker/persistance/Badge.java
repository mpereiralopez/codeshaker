package mpereira.dev.devshaker.persistance;

import android.provider.BaseColumns;

import java.io.Serializable;

/**
 * Created by mpereira on 14/3/15.
 */
public class Badge implements Serializable{

    private Long ID;
    private String name;
    private String img;

    public Long getID() {
        return ID;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public Badge(Long ID, String name, String img) {
        this.ID = ID;
        this.name = name;
        this.img = img;
    }

    public static abstract class BadgeEntry implements BaseColumns {
        public static final String TABLE_NAME = "badge";
        public static final String COLUMN_NAME_BADGE_ID = "Id";
        public static final String COLUMN_NAME_BADGE_NAME = "name";
        public static final String COLUMN_NAME_BADGE_IMG = "img";
    }
}
