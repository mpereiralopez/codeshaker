package mpereira.dev.devshaker.level;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toolbar;

import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import mpereira.dev.devshaker.MainMenu;
import mpereira.dev.devshaker.R;
import mpereira.dev.devshaker.SingInActivity;
import mpereira.dev.devshaker.Utils.ExperienceCalculator;
import mpereira.dev.devshaker.Utils.Utils;
import mpereira.dev.devshaker.connection.Connections;
import mpereira.dev.devshaker.persistance.DevShackerDbHelper;
import mpereira.dev.devshaker.persistance.Level;
import mpereira.dev.devshaker.persistance.Subcategory;
import mpereira.dev.devshaker.persistance.User;

/**
 * Created by mpereira on 11/4/15.
 */
public class LevelCompleted extends Activity {

    private TextView percentageComplete;
    private List<Boolean> listaRespuestasTest;
    private LinearLayout responseStatusContainer;
    private TextView lvlCompletedHeader;
    private TextView lvlcompleted;
    private boolean isFromTutorial;
    private Subcategory subcategory;
    private Level level;
    private static DevShackerDbHelper dbhelper;
    private static  SQLiteDatabase db;
    private int percentage;
    private Context ctx;

    private ProgressBar progressBar;
    private TextView progressNumber;
    private TextView xpObtainedTextView;


    //LevelCompleted levelCompleted = new LevelCompleted(int percentageComplete, );

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.level_complete);
        ctx = this;
        int percentCorrect = 0;
        int aux = 0;
        lvlCompletedHeader = (TextView) findViewById(R.id.lvlCompletedHeader);
        lvlcompleted = (TextView) findViewById(R.id.lvlcompleted);
        progressBar = (ProgressBar) findViewById(R.id.progressBar2);
        progressNumber = (TextView) findViewById(R.id.progressNumber);
        xpObtainedTextView = (TextView) findViewById(R.id.xpobtained);
        Bundle extras = getIntent().getExtras();
        int userXp = 0;
        User u = null;
        dbhelper = new DevShackerDbHelper(this);
        db =  dbhelper.getReadableDatabase();
        if (extras != null) {
            boolean [] list_answerses = extras.getBooleanArray("LIST_ANSWERS");
            listaRespuestasTest = new LinkedList<Boolean>();
            for(int i=0; i<list_answerses.length;i++){
                listaRespuestasTest.add(list_answerses[i]);
                if(list_answerses[i]==true)aux = aux+1;
            }
            percentCorrect = (aux*100)/listaRespuestasTest.size();
            isFromTutorial = extras.getBoolean("isFromTutorial");
            lvlCompletedHeader.setText(extras.getString("LevelCompletedHeader"));
            String lvl = "";
            if(isFromTutorial){
                lvl = "Tutorial";
            }else{
                lvl = extras.getString("lvlcompleted");
                u  = User.getUserFromDDBB(db);
                userXp = u.getXp();

            }
            level = (Level)getIntent().getSerializableExtra("Level");
            lvlcompleted.setText("Level "+lvl+" completed");
            subcategory =(Subcategory)getIntent().getSerializableExtra("subcategory");

        }



        percentageComplete = (TextView)findViewById(R.id.percentageComplete);
        responseStatusContainer = (LinearLayout) findViewById(R.id.responseStatusContainer);
        Iterator<Boolean> it = listaRespuestasTest.iterator();
        int index = 0;
        while(it.hasNext()){
            index ++;
            boolean status = it.next();
            HashMap<String,Object> map = new HashMap<>();
            map.put(Utils.LEVEL_COMMPLETED_KEY,status);
            TextView textView = Utils.createResponseCircle(getApplicationContext(), getResources(), map, Integer.toString(index),Utils.answerStatusRadious);
            responseStatusContainer.addView(textView);

        }

        this.percentage = percentCorrect;
        percentageComplete.setText(percentCorrect + "%");

        //COSAS PARA LA BARRA DE PROGRESO


        int diff = 0;
        if(level!=null){
            diff = Integer.parseInt(level.getName());
        }

        int xpObtained = ExperienceCalculator.getExperienceByLevelAndPercentage(diff,percentCorrect);
        xpObtainedTextView.setText("+"+xpObtained+" XP");
        userXp = userXp + xpObtained;
        int [] values = ExperienceCalculator.getLevelOfProgressionOfUser(userXp);

        System.out.println(values[0] + "  "+ values[1]);
        progressNumber.setText(values[0]+"");
        int progressForBar = (userXp*100)/values[1];


        progressBar.setProgress(progressForBar);

        if(!isFromTutorial) User.updateUserXp(u.getID(),userXp,db);



    }


    public void goToMenu(View view){
        //System.out.println("Button pressed");
        if(isFromTutorial){
            /*Intent mainIntent = new Intent(LevelCompleted.this,MainMenu.class);
            LevelCompleted.this.startActivity(mainIntent);
            LevelCompleted.this.finish();*/
            //Ahora tengo que ir a crearme la cuenta.

            Intent mainIntent = new Intent(LevelCompleted.this,SingInActivity.class);
            LevelCompleted.this.startActivity(mainIntent);
            LevelCompleted.this.finish();

        }else{
            Log.d("LevelCompleted", level.getID() + "");
            Level.updateLevelStatusAndPercentage(level.getID(),this.percentage,db);

            if(Connections.haveNetworkConnection(this.ctx)){
                System.out.println("Tengo conexion a internet");
                Bundle extras = getIntent().getExtras();
                User u  = User.getUserFromDDBB(db);
                int userXp = u.getXp();
                String param0 = subcategory.getId()+"_"+level.getID()+"_";
                String param1 = this.percentage+"#";
                String param2 = "50"; //xp calculated;
                Connections conn = new Connections();
                String [] params = {param0,param1,param2};
                conn.callToWS(Connections.UPDATE_LEVEL_FINISHED,params,u.getID());
            }


            Intent mainIntent = new Intent(LevelCompleted.this,LevelActivity.class);
            mainIntent.putExtra("Subcategory",subcategory);
            LevelCompleted.this.startActivity(mainIntent);
            LevelCompleted.this.finish();
        }

    }


}
