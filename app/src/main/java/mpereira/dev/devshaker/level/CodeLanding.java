package mpereira.dev.devshaker.level;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.Iterator;
import java.util.List;

import mpereira.dev.devshaker.R;
import mpereira.dev.devshaker.Utils.MenuUtils;
import mpereira.dev.devshaker.connection.Connections;
import mpereira.dev.devshaker.persistance.DevShackerDbHelper;
import mpereira.dev.devshaker.persistance.Level;
import mpereira.dev.devshaker.persistance.Subcategory;

/**
 * Created by mpereira on 11/4/15.
 */
public class CodeLanding extends ActionBarActivity implements View.OnClickListener {

    private static final String TAG= "CODELANDING.java";
    private static List<Subcategory> subcategoryList;
    private static DevShackerDbHelper dbhelper;
    private static TableLayout tableLayout;
    private static ActionBarActivity act;
    private static Context ctx;
    private static CodeLanding codeLanding;
    private static ProgressDialog progress;
    private static ProgressBar mProgress;


    //LevelCompleted levelCompleted = new LevelCompleted(int percentageComplete, );

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.code_landing);
        tableLayout =(TableLayout)findViewById(R.id.code_table);

        this.setTitle("Code");

        ctx = this;
        act = this;
        codeLanding = this;

        MenuUtils utils = new MenuUtils();
        ActionBarActivity act = this;
        utils.setLeftMenu(ctx, act);

        dbhelper = new DevShackerDbHelper(this);
        SQLiteDatabase db =  dbhelper.getReadableDatabase();


        subcategoryList =  Subcategory.getListSubcategoryFromDB(1, db);

        if(subcategoryList.size()==0){
            System.out.println("No hay Subcaterias, hay que descargarlas");

            progress = ProgressDialog.show(this, "Downloading",
                    "Downloading subcategories from Server, please wait", true);
            Connections connections = new Connections();
            connections.callToWS(Connections.SUBCATEGORY, null,0);




        }else{
            String selectQuery = "SELECT * FROM " + Level.LevelEntry.TABLE_NAME+";";
            Log.d(TAG, selectQuery);
            Cursor cursor = db.rawQuery(selectQuery, null);
            System.out.println(cursor.getCount());

            setSubcategoryLayout(db, ctx, this);

        }
    }

    public static void setSubCategoriesFormJSON(JSONArray jsonArray){
        Subcategory subcategory=null;
        Gson gson = new Gson();
        SQLiteDatabase db = dbhelper.getWritableDatabase();

        for(int i=0; i<jsonArray.length();i++){

            try {
                subcategory = gson.fromJson(jsonArray.getJSONObject(i).toString(), Subcategory.class);
                long val = Subcategory.saveSubcategoryToDDBB(subcategory, db);
                System.out.println(val);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        subcategoryList =  Subcategory.getListSubcategoryFromDB(1, db);

        Connections connections = new Connections();
        connections.callToWS(Connections.LEVEL, null,0);
    }

    public static void setLevelsFormJSON(JSONArray jSONArray){
        Gson gson = new Gson();
        SQLiteDatabase db = dbhelper.getWritableDatabase();
        Level level = null;
        for(int i=0; i<jSONArray.length();i++){

            try {
                level = gson.fromJson(jSONArray.getJSONObject(i).toString(), Level.class);
                long val = Level.saveLevelsToDDBB(level, db);
                System.out.println(level.getSubCategory()+" "+level.getName()+" "+level.getNumberOfQuestions()+" "+level.getID());

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        progress.dismiss();
        setSubcategoryLayout(db, ctx, codeLanding);
    }

    private static void  setSubcategoryLayout(SQLiteDatabase db, Context ctx, CodeLanding act){
        Iterator<Subcategory> it = subcategoryList.iterator();
        int index = 0;
        TableRow row=null ;
        int resto = subcategoryList.size()%3;

        while(it.hasNext()){
            Log.d(TAG,"INDEX: "+index);
            Subcategory sub = it.next();
            if(index%3 == 0){
                row = new TableRow(ctx);
                row.setId(index);
                row.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT));
                tableLayout.addView(row,new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));
            }

            index = index+1;

            LinearLayout linearLayout = new LinearLayout(ctx);
            linearLayout.setOrientation(LinearLayout.VERTICAL);
            TableRow.LayoutParams params = new TableRow.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT);
            params.weight=1;
            linearLayout.setLayoutParams(params);



            Button btn = new Button(ctx);
            btn.setId((int) sub.getId());
            btn.setBackgroundDrawable(ctx.getResources().getDrawable(R.drawable.circle_code));
            TableRow.LayoutParams btnLP = new TableRow.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);

            btnLP.gravity= Gravity.CENTER;
            btnLP.setMargins(0, 40, 0, 0);
            btn.setId((int) sub.getId());
            btn.setLayoutParams(btnLP);
            btn.setMaxWidth(120);
            btn.setMaxHeight(120);
            btn.setText(sub.getName());
            btn.setTextColor(Color.WHITE);

            linearLayout.addView(btn);

            btn.setOnClickListener(act);


            TextView textview = new TextView(ctx);

            TableRow.LayoutParams textViewLP =   new TableRow.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 30);
            textview.setGravity(Gravity.CENTER);
            textview.setLayoutParams(textViewLP);
            //Hueco para tener el level:
            System.out.println(dbhelper.getTableAsString(db,Level.LevelEntry.TABLE_NAME));
            Level levelValue = Level.getLastLevelOfSubcategory(sub.getId(), db);
            int numOfLevelsOfSubcategory = Level.getTotalLevelsOfSubcategory(sub.getId(), db);
            System.out.println("numOfLevelsOfSubcategory "+numOfLevelsOfSubcategory);
            String formattedLevel = levelValue.getName();

            textview.setText("Level "+formattedLevel+"/"+numOfLevelsOfSubcategory);
            textview.setTextSize(16);

            linearLayout.addView(textview);


            row.addView(linearLayout);
        }

        //Relleno con vacios
            for(int i=resto; i<3;i++){
                LinearLayout linearLayout = new LinearLayout(ctx);
                linearLayout.setOrientation(LinearLayout.VERTICAL);
                TableRow.LayoutParams params = new TableRow.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT);
                params.weight=1;
                linearLayout.setLayoutParams(params);
                row.addView(linearLayout);
            }


    }




    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        Subcategory subcategorySelected = null;
        for(int i=0; i<subcategoryList.size();i++){
            if(subcategoryList.get(i).getId() == v.getId()){
                subcategorySelected =subcategoryList.get(i);
                break;
            }
        }
        System.out.println(subcategorySelected.getName());
        Intent mainIntent = new Intent(CodeLanding.this,LevelActivity.class);
        mainIntent.putExtra("Subcategory",subcategorySelected);
        CodeLanding.this.startActivity(mainIntent);

    }

}