package mpereira.dev.devshaker.level;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.List;

import mpereira.dev.devshaker.NavDrawerItem;
import mpereira.dev.devshaker.QuestionTestActivity;
import mpereira.dev.devshaker.R;
import mpereira.dev.devshaker.Utils.MenuUtils;
import mpereira.dev.devshaker.persistance.DevShackerDbHelper;
import mpereira.dev.devshaker.persistance.Level;
import mpereira.dev.devshaker.persistance.Subcategory;

/**
 * Created by mpereira on 5/6/15.
 */
public class LevelActivity extends ActionBarActivity implements View.OnClickListener {

    private Subcategory subcategory;
    private List<Level> totalLevels;
    private Level level;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_levels);

        //GET PARAMETERS
        subcategory =(Subcategory)getIntent().getSerializableExtra("Subcategory");



        MenuUtils utils = new MenuUtils();
        ActionBarActivity act = this;
        Context ctx = this;
        utils.setLeftMenu(ctx, act);


        DevShackerDbHelper dbhelper = new DevShackerDbHelper(this);
        SQLiteDatabase db =  dbhelper.getReadableDatabase();
        boolean levelStarted = false;

        level = Level.getLastLevelOfSubcategory(subcategory.getId(), db);

        totalLevels = Level.getTotalLevelOfSubcategory(subcategory.getId(), db);
        System.out.println("Actual Level in Subcategory: "+level.getName());
        this.setTitle(subcategory.getName());
        TableRow row=null;
        TableLayout table = (TableLayout)findViewById(R.id.levels_table);
        Button btn = null;
        TextView txt = null;
        for(int i=0; i<totalLevels.size();i++){
            row= (TableRow)table.getChildAt(i);
            row.setVisibility(View.VISIBLE);

            btn = (Button)row.getChildAt(0);
            txt = (TextView)row.getChildAt(1);
            btn.setText(String.format("%02d", i+1));
            txt.setText(totalLevels.get(i).getPercentage()+"% done");


            if(i+1<Integer.parseInt(level.getName())){
                btn.setBackgroundDrawable(getResources().getDrawable(R.drawable.lvcomp));
                btn.setTextColor(Color.WHITE);
            }
            if(i+1 == Integer.parseInt(level.getName())){
                btn.setBackgroundDrawable(getResources().getDrawable(R.drawable.lvincomp));
                btn.setTextColor(getResources().getColor(R.color.blue_menu));
                btn.setOnClickListener(this);

            }

            if(i+1 > Integer.parseInt(level.getName())){
                btn.setBackgroundDrawable(getResources().getDrawable(R.drawable.lvbloc));
                btn.setTextColor(getResources().getColor(R.color.blue_nostarted));
            }

        }
    }


    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        int levelClicked = v.getId();
        Intent mainIntent = new Intent(LevelActivity.this,QuestionTestActivity.class);
        mainIntent.putExtra("Subcategory",subcategory);
        mainIntent.putExtra("Level",level);
        mainIntent.putExtra("totalLevels",totalLevels.size());
        mainIntent.putExtra("IS_FROM_TUTORIAL",false);
        LevelActivity.this.startActivity(mainIntent);

    }


}
