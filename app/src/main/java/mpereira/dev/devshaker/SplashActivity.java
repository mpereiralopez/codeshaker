package mpereira.dev.devshaker;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.preference.Preference;
import android.provider.BaseColumns;
import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import mpereira.dev.devshaker.connection.Connections;
import mpereira.dev.devshaker.persistance.Category;
import mpereira.dev.devshaker.persistance.DevShackerDbHelper;
import mpereira.dev.devshaker.persistance.Subcategory;
import mpereira.dev.devshaker.tutorial.TutorialActivity;


public class SplashActivity extends Activity {

    private final int SPLASH_DURATION = 3000;
    static Context context;
    static SharedPreferences prefs;
    private static DevShackerDbHelper dbhelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        /* New Handler to start the Menu-Activity
         * and close this Splash-Screen after some seconds.*/
        prefs =this.getSharedPreferences("mpereira.dev.devshaker", MODE_PRIVATE);
        context = this;
        dbhelper = new DevShackerDbHelper(this);

        new Handler().postDelayed(new Runnable() {

            public void run() {

                Log.d("prefs","prefs");
                if (prefs.getBoolean("firstrun", true)) {
                    // Do first run stuff here then set 'firstrun' as false
                    // using the following line to edit/commit prefs

                    Log.d("FirstRun", "True");
                    Intent mainIntent = new Intent(SplashActivity.this,TutorialActivity.class);
                    SplashActivity.this.startActivity(mainIntent);
                    SplashActivity.this.finish();
                }else{
                    Log.d("FirstRun", "False");
                    Intent mainIntent = new Intent(SplashActivity.this,MainMenu.class);
                    SplashActivity.this.startActivity(mainIntent);
                    SplashActivity.this.finish();
                }



            }
        }, SPLASH_DURATION);

        //Connections connections = new Connections();
        //connections.callToWS(Connections.SUBCATEGORY, null);
    }

    public static void setCategories(){
        ContentValues initialValues = new ContentValues();

        Gson gson = new Gson();
        Subcategory subcategory = null;

        //Primero las categorias que son fijas
        Category cat1 = new Category(1,"Code");
        Category cat2 = new Category(2,"Build");
        Category cat3 = new Category(3,"Think");

        SQLiteDatabase db = dbhelper.getWritableDatabase();

        initialValues.put(Category.CategoryEntry.COLUMN_NAME_CATEGORY_ID, cat1.getId());
        initialValues.put(Category.CategoryEntry.COLUMN_NAME_CATEGORY_NAME, cat1.getName());

        db.insertWithOnConflict(Category.CategoryEntry.TABLE_NAME,
                Category.CategoryEntry.COLUMN_NAME_CATEGORY_ID, initialValues, SQLiteDatabase.CONFLICT_REPLACE);
        initialValues.clear();

        initialValues.put(Category.CategoryEntry.COLUMN_NAME_CATEGORY_ID, cat2.getId());
        initialValues.put(Category.CategoryEntry.COLUMN_NAME_CATEGORY_NAME, cat2.getName());

        db.insertWithOnConflict(Category.CategoryEntry.TABLE_NAME,
                Category.CategoryEntry.COLUMN_NAME_CATEGORY_ID, initialValues, SQLiteDatabase.CONFLICT_REPLACE);
        initialValues.clear();

        initialValues.put(Category.CategoryEntry.COLUMN_NAME_CATEGORY_ID, cat3.getId());
        initialValues.put(Category.CategoryEntry.COLUMN_NAME_CATEGORY_NAME, cat3.getName());

        db.insertWithOnConflict(Category.CategoryEntry.TABLE_NAME,
                Category.CategoryEntry.COLUMN_NAME_CATEGORY_ID, initialValues, SQLiteDatabase.CONFLICT_REPLACE);
        initialValues.clear();

        Log.d("Splash", "Categorias anadidas correctamente");

    }

}
