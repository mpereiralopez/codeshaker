package mpereira.dev.devshaker;

import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.TextView;

import de.hdodenhof.circleimageview.CircleImageView;
import mpereira.dev.devshaker.Utils.MenuUtils;
import mpereira.dev.devshaker.persistance.DevShackerDbHelper;
import mpereira.dev.devshaker.persistance.User;

/**
 * Created by mpereira on 5/6/15.
 */
public class BadgesActivity extends ActionBarActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.badges);

        TextView userNameLabel = (TextView)findViewById(R.id.badge_user_name);

        DevShackerDbHelper dbhelper = new DevShackerDbHelper(this);
        SQLiteDatabase db =  dbhelper.getReadableDatabase();
        User user = User.getUserFromDDBB(db);

        userNameLabel.setText(user.getDisplayName());
        MenuUtils utils = new MenuUtils();
        utils.setLeftMenu(this, this);

        CircleImageView circleImageView = (CircleImageView) findViewById(R.id.profile_circle_pic);
        if(user.getImg().length()!=0){
            Bitmap bm;
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(user.getImg(), options);
            final int REQUIRED_SIZE = 200;
            int scale = 1;
            while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                    && options.outHeight / scale / 2 >= REQUIRED_SIZE)
                scale *= 2;
            options.inSampleSize = scale;
            options.inJustDecodeBounds = false;
            bm = BitmapFactory.decodeFile(user.getImg(), options);
            circleImageView.setImageBitmap(bm);
        }
    }


}
